﻿using e_commerse.usermanagement.service.Database.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace e_commerse.usermanagement.service.Services
{
    public interface IAuthService
    {
        string GenerateTokenString(LoginUser user);
        Task<IdentityUser> GetProfile(HttpRequest request);
        Task<bool> Login(LoginUser user);
        Task<bool> RegisterUser(User user);
    }
}