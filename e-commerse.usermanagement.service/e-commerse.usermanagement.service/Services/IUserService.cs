﻿using e_commerse.usermanagement.service.Database.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace e_commerse.usermanagement.service.Services
{
    public interface IUserService
    {
        Task<bool> DeleteUser(string id);
        Task<List<IdentityUser>> GetUser();
        Task<IdentityUser> GetUserById(string id);
        Task<bool> PutUser(string id, User user);
    }
}