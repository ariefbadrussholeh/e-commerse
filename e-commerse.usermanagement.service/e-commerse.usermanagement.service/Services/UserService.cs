﻿using e_commerse.usermanagement.service.Database;
using e_commerse.usermanagement.service.Database.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace e_commerse.usermanagement.service.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly AppDbContext _appDbContext;

        public UserService(UserManager<IdentityUser> userManager, AppDbContext appDbContext)
        {
            _userManager = userManager;
            _appDbContext = appDbContext;
        }

        public async Task<bool> DeleteUser(string id)
        {
            var identityUser = await _userManager.FindByIdAsync(id);

            if (identityUser == null) 
            {
                return false;
            }

            var result = await _userManager.DeleteAsync(identityUser);
            return result.Succeeded;
        }

        public async Task<List<IdentityUser>> GetUser()
        {
            return await _appDbContext.Users.ToListAsync();
        }

        public async Task<IdentityUser> GetUserById(string id)
        {
            var user = await _appDbContext.Users.FindAsync(id);

            if (user == null)
            {
                return null;
            }

            return user;
        }

        public async Task<bool> PutUser(string id, User user)
        {
            var identityUser = await _userManager.FindByIdAsync(id);

            if (identityUser == null)
            {
                return false;
            }

            identityUser.UserName = user.UserName;
            identityUser.Email = user.Email;
            identityUser.PhoneNumber = user.PhoneNumber;

            var result = await _userManager.UpdateAsync(identityUser);

            return result.Succeeded;
        }
        private bool UserExist(string id)
        {
            return _appDbContext.Users.Any(e => e.Id == id);
        }
    }
}
