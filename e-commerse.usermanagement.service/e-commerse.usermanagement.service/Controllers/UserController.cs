﻿using e_commerse.usermanagement.service.Database.Models;
using e_commerse.usermanagement.service.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace e_commerse.usermanagement.service.Controllers
{
    [Route("User/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult> GetUser()
        {
            return Ok(new
            {
                StatusCode = HttpStatusCode.OK,
                Message = "Success",
                Data = await _userService.GetUser()
            });
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetUserById(string id)
        {
            var result = await _userService.GetUserById(id);

            if (result == null)
            {
                return NotFound(new
                {
                    StatusCode = HttpStatusCode.NotFound,
                    Message = "Something went wrong"
                });
            }
            return Ok(new
            {
                StatusCode = HttpStatusCode.OK,
                Message = "Success",
                Data = result
            });
        }

        [HttpPut]
        public async Task<ActionResult> PutUser(string id, User user)
        {
            if (await _userService.PutUser(id, user))
            {
                return Ok(new
                {
                    StatusCode = HttpStatusCode.OK,
                    Message = "Update Succesfull"
                });
            }
            return BadRequest(new
            {
                StatusCode = HttpStatusCode.BadRequest,
                Message = "Something went wrong"
            });
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteUser(string id)
        {
            if (await _userService.DeleteUser(id))
            {
                return Ok(new
                {
                    StatusCode = HttpStatusCode.OK,
                    Message = "Delete Succesfull"
                });
            }
            return BadRequest(new
            {
                StatusCode = HttpStatusCode.BadRequest,
                Message = "Something went wrong"
            });
        }
    }
}
