﻿using e_commerse.usermanagement.service.Database.Models;
using e_commerse.usermanagement.service.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Threading.Tasks;

namespace e_commerse.usermanagement.service.Controllers
{
    [Route("User/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService= authService;
        }

        [HttpPost("Register")]
        [AllowAnonymous]
        public async Task<ActionResult> RegisterUser(User user)
        {
            if(await _authService.RegisterUser(user))
            {
                return Ok(new
                {
                    StatusCode = HttpStatusCode.OK,
                    Message = "Register Succesfully",
                });
            }
            return BadRequest(new 
            {
                StatusCode = HttpStatusCode.BadRequest,
                Message = "Something went wrong",
            });
        }

        [HttpPost("Login")]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginUser user)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Message = "Something went wrong",
                });
            }

            if(await _authService.Login(user))
            {
                var tokenString = _authService.GenerateTokenString(user);
                return Ok(new
                {
                    StatusCode = HttpStatusCode.OK,
                    Message = "Login Succesfully",
                    Token = tokenString
                });
            }
            return BadRequest(new
            {
                StatusCode = HttpStatusCode.BadRequest,
                Message = "Something went wrong",
            });
        }

        [HttpGet]
        [Route("Profile")]
        [Authorize]
        public async Task<ActionResult> GetProfile()
        {
            var result = await _authService.GetProfile(Request);
            
            if (result == null)
            {
                return BadRequest(new
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Message = "Something went wrong",
                });
            }

            return Ok(new
            {
                StatusCode = HttpStatusCode.OK,
                Message = "Success",
                Data = result
            });
        }
    }
}
