﻿using e_commerse.usermanagement.service.Database.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace e_commerse.usermanagement.service.Database
{
    public class AppDbContext : IdentityDbContext
    {
        public AppDbContext(DbContextOptions options) : base(options) { }

        //public DbSet<Example> Example { get; set; }

        //public DbSet<User> users { get; set; }
    }
}
