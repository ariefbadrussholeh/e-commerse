﻿namespace e_commerse.usermanagement.service.Database.Models
{
    public class LoginUser
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
