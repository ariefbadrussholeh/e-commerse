﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using e_commerse.masterdata.service.Database;
using e_commerse.masterdata.service.Database.Models;
using Microsoft.AspNetCore.Authorization;
using System.Net;

namespace e_commerse.masterdata.service.Controllers
{
    [Route("datamaster/[controller]")]
    [ApiController]
    [Authorize]
    public class CategoriesController : ControllerBase
    {
        private readonly AppDbContext _context;

        public CategoriesController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Categories
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Category>>> GetCategories()
        {
            var result = await _context.Categories.ToListAsync();

            return Ok(new
            {
                StatusCode = HttpStatusCode.OK,
                Message = "Success",
                Data = result
            });
        }

        // GET: api/Categories/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Category>> GetCategory(string id)
        {
            var category = await _context.Categories.FindAsync(id);

            if (category == null)
            {
                return NotFound(new
                {
                    StatusCode = HttpStatusCode.NotFound,
                    Message = "Something went wrong"
                });
            }

            return Ok(new
            {
                StatusCode = HttpStatusCode.OK,
                Message = "Success",
                Data = category
            });
        }

        // PUT: api/Categories/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCategory(string id, Category category)
        {
            if (id != category.Id)
            {
                return BadRequest(new 
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Message = "Something went wrong"
                });
            }

            _context.Entry(category).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound(new
                    {
                        StatusCode = HttpStatusCode.NotFound,
                        Message = "Something went wrong"
                    });
                }
                else
                {
                    throw;
                }
            }

            return Ok(new
            {
                StatusCode = HttpStatusCode.OK,
                Message = "Update Category Succesfull"
            });
        }

        // POST: api/Categories
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Category>> PostCategory(Category category)
        {
            _context.Categories.Add(category);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CategoryExists(category.Id))
                {
                    return Conflict(new 
                    {
                        StatusCode = HttpStatusCode.Conflict,
                        Message = "Something went wrong"
                    });
                }
                else
                {
                    throw;
                }
            }

            return Ok(new
            {
                StatusCode = HttpStatusCode.OK,
                Message = "Post Category Succesfull"
            });
        }

        // DELETE: api/Categories/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Category>> DeleteCategory(string id)
        {
            var category = await _context.Categories.FindAsync(id);
            if (category == null)
            {
                return NotFound(new
                {
                    StatusCode = HttpStatusCode.NotFound,
                    Message = "Something went wrong"
                });
            }

            _context.Categories.Remove(category);
            await _context.SaveChangesAsync();

            return Ok(new
            {
                StatusCode = HttpStatusCode.OK,
                Message = "Delete Categories Succesfull"
            });
        }

        private bool CategoryExists(string id)
        {
            return _context.Categories.Any(e => e.Id == id);
        }
    }
}
