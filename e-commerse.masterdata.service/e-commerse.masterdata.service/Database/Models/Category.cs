﻿using System;
using System.ComponentModel.DataAnnotations;

namespace e_commerse.masterdata.service.Database.Models
{
    public class Category
    {
        [Key]
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public void SetCreated()
        {
            Id = Guid.NewGuid().ToString();
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }
        public void SetUpdated()
        {
            UpdatedAt = DateTime.Now;
        }
    }
}
