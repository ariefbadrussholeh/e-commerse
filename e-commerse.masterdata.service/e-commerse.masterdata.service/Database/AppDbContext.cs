﻿using e_commerse.masterdata.service.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace e_commerse.masterdata.service.Database
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options) { }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
