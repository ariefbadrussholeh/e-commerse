﻿namespace e_commerse.transaction.service.ViewModels
{
    public class ItemsViewModel
    {
        public string ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal Discount { get; set; }
    }
}
