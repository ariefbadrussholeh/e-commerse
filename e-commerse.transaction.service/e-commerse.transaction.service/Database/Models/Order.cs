﻿using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace e_commerse.transaction.service.Database.Models
{
    public class Order
    {
        [Key]
        public string Id { get; set; }
        public string UserId { get; set; }
        public decimal Total { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        public void SetCreated()
        {
            CreatedAt = DateTime.Now;
            UpdatedAt = DateTime.Now;
        }
        public void SetUpdated()
        {
            UpdatedAt = DateTime.Now;
        }
    }
}
