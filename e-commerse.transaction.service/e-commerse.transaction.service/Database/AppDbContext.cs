﻿using e_commerse.transaction.service.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace e_commerse.transaction.service.Database
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options) : base(options) { }

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItems> OrderItems { get; set; }
    }
}
