﻿using e_commerse.transaction.service.Database.Models;
using e_commerse.transaction.service.ViewModels;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace e_commerse.transaction.service.Services
{
    public interface IOrderService
    {
        Task<bool> AddOrderItems(OrderViewModel orderViewModel, HttpRequest request, string orderId);
        Task<string> CreateOrder(HttpRequest request);
        Task<bool> DeleteOrder(string id);
        Task<Order> GetOrderById(string id);
        Task<IEnumerable<Order>> GetOrders();
    }
}
