﻿using e_commerse.transaction.service.Database;
using e_commerse.transaction.service.Database.Models;
using e_commerse.transaction.service.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Headers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace e_commerse.transaction.service.Services
{
    public class OrderService : IOrderService
    {
        private readonly AppDbContext _appDbContext;

        public OrderService(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public async Task<bool> AddOrderItems(OrderViewModel orderViewModel, HttpRequest request, string orderId)
        {
            try
            {
                string jwt = request.Headers.Where(x => x.Key.ToLower() == "authorization")?.FirstOrDefault().Value.ToString();

                decimal totalAmount = 0;
                foreach (ItemsViewModel item in orderViewModel.Items)
                {
                    var productData = RestGet("https://localhost:5002", $"/datamaster/Products/{item.ProductId}", jwt);

                    var price = productData.GetProperty("price").GetDecimal();
                    var discount = item.Discount;
                    var quantity = item.Quantity;
                    var total = price * quantity;

                    OrderItems orderItems = new OrderItems();
                    orderItems.OrderId = orderId;
                    orderItems.ProductId = item.ProductId;
                    orderItems.ProductName = productData.GetProperty("name").GetString();
                    orderItems.Price = price;
                    orderItems.Discount = discount;
                    orderItems.Quantity = quantity;
                    orderItems.Amount = total * (1 - discount / 100);

                    totalAmount += orderItems.Amount;

                    _appDbContext.OrderItems.Add(orderItems);
                    await _appDbContext.SaveChangesAsync();
                }

                Order order = await GetOrderById(orderId);
                order.Total = totalAmount;
                _appDbContext.Entry(order).State = EntityState.Modified;
                await _appDbContext.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<string> CreateOrder(HttpRequest request)
        {
            try
            {
                string jwt = request.Headers.Where(x => x.Key.ToLower() == "authorization")?.FirstOrDefault().Value.ToString();
                var userData = RestGet("https://localhost:5001", $"/User/Auth/Profile", jwt);

                var userId = userData.GetProperty("id").GetString();
                var orderId = Guid.NewGuid().ToString();
                decimal totalAmount = 0;

                Order order = new Order();
                order.Id = orderId;
                order.UserId = userId;
                order.Total = totalAmount;

                _appDbContext.Orders.Add(order);
                await _appDbContext.SaveChangesAsync();

                return orderId;
            } catch(Exception ex)
            {
                return null;
            }
        }

        public async Task<bool> DeleteOrder(string id)
        {
            var orderItems = _appDbContext.OrderItems
                                         .Where(oi => oi.OrderId == id);

            if (orderItems != null)
            {
                _appDbContext.OrderItems.RemoveRange(orderItems);
            }

            var order = await _appDbContext.Orders.FindAsync(id);
            
            if (order == null)
            {
                return false;
            }

            _appDbContext.Orders.Remove(order);
            await _appDbContext.SaveChangesAsync();

            return true;
        }

        public async Task<Order> GetOrderById(string id)
        {
            return await _appDbContext.Orders.FindAsync(id);
        }

        public async Task<IEnumerable<Order>> GetOrders()
        {
            return await _appDbContext.Orders.ToListAsync();
        }

        public JsonElement RestGet(string clientUrl, string endPoint, string jwt)
        {
            var client = new RestClient(clientUrl);
            var request = new RestRequest(endPoint, Method.GET);
            request.AddParameter("Authorization", jwt, ParameterType.HttpHeader);
            var queryResult = client.Execute(request).Content;

            var jsonDoc = JsonDocument.Parse(queryResult);
            var data = jsonDoc.RootElement.GetProperty("data");

            return data;
        }
    }
}
