﻿using e_commerse.transaction.service.Database.Models;
using e_commerse.transaction.service.Services;
using e_commerse.transaction.service.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using System.Net;
using System.Threading.Tasks;

namespace e_commerse.transaction.service.Controllers
{
    [Route("Transaction/[controller]")]
    [ApiController]
    [Authorize]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost]
        public async Task<ActionResult> Order(OrderViewModel orderViewModel)
        {
            var orderId = await _orderService.CreateOrder(Request);
            if (orderId == null)
            {
                return BadRequest(new
                {
                    StatusCode = HttpStatusCode.BadRequest,
                    Message = "Something went wrong"
                });
            }

            if(await _orderService.AddOrderItems(orderViewModel, Request, orderId))
            {
                return Ok(new
                {
                    StatusCode = HttpStatusCode.OK,
                    Message = "Order Succesfull",
                    OrderId = orderId
                });
            }

            return BadRequest(new
            {
                StatusCode = HttpStatusCode.BadRequest,
                Message = "Something went wrong"
            });
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetOrder(string id)
        {
            var result = await _orderService.GetOrderById(id);
            if (result != null)
            {
                return Ok(new
                {
                    StatusCode = HttpStatusCode.OK,
                    Message = "Success",
                    Data = result
                });
            }

            return BadRequest(new
            {
                StatusCode = HttpStatusCode.BadRequest,
                Message = "Something went wrong"
            });
        }

        [HttpGet]
        public async Task<ActionResult> GetOrders()
        {
            var result = await _orderService.GetOrders();
            if (result != null)
            {
                return Ok(new
                {
                    StatusCode = HttpStatusCode.OK,
                    Message = "Succes",
                    Data = result
                });
            }

            return BadRequest(new
            {
                StatusCode = HttpStatusCode.BadRequest,
                Message = "Something went wrong"
            });
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteOrder(string id)
        {
            if (await _orderService.DeleteOrder(id))
            {
                return Ok(new
                {
                    StatusCode = HttpStatusCode.OK,
                    Message = "Delete Order Succesfull"
                });
            }

            return BadRequest(new
            {
                StatusCode = HttpStatusCode.BadRequest,
                Message = "Something went wrong"
            });
        }

    }
}
